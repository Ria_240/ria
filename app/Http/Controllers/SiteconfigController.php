<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SiteconfigController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('siteconfig');
    }
	
	public function save_site(Request $request) 
	{
			/*$firstname = $request->input('first_name');
			 $lastname = $request->input('last_name');
			 $address = $request->input('address');
			 $email = $request->input('email');
			 $data=array('first_name'=>$firstname,"last_name"=>$lastname,"address"=>$address,"email"=>$email);
			 DB::table('first_laravel')->insert($data);*/
			 $inputs = $request->all(); 			 
			 if($inputs){
				 foreach($inputs as $key=>$value){  
					 if($value){
					$res = DB::table('site_config')->where('option_name', $key)->count();
					if($res>0){ 
						DB::table('site_config')
						->where('option_name', $key)
						->update(['option_name' => $key, 'option_value' => $value]);
					}else{
						DB::table('site_config')->insert(
							['option_name' => $key, 'option_value' => $value]
						);
					} 
				}
				 }
					
			 return view('siteconfig',compact('site_title','logo','facebook','twitter','linkedin','head_office_address','contact_email'));
			 }
			 else{
			 return redirect('siteconfig');
			 }
	}
}
