<?php echo $__env->make('components.mainmenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container-fluid">
	<!-- ============================================================== -->
	 <div class="row">
                <div class="col-sm-8 ">
                 
					<!--form method="POST" action="http://3.6.109.8/doctel/admin/postSettings" accept-charset="UTF-8" enctype="multipart/form-data" class="form-horizontal mt-12" id="settingsform" role="form"><input name="_token" type="hidden" value="wQ3Lnd1hX07survzEjo65gG16HYaEThCozH3fFm4"-->
					 
					<ul class="nav nav-tabs" role="tablist">
					  <li class="nav-item">
						<a class="nav-link active" href="#setting" role="tab" data-toggle="tab">Site Settings</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="#social" role="tab" data-toggle="tab">Social Links</a>
					  </li>  
					  <li class="nav-item">
						<a class="nav-link" href="#address" role="tab" data-toggle="tab">Contact</a>
					  </li>
					
					</ul>
 

					<!-- Tab panes -->
					<div class="tab-content">
					  <div role="tabpanel" class="tab-pane active" id="setting">
						<h3>Site Settings</h3>
						 <?php echo Form::open(array('url' => array('/siteconfig'),'class'=>'','name'=>'award_form','id'=>'award_form','role'=>'form','enctype' => 'multipart/form-data')); ?>

					   <div class="form-group">
						<label for="title">Site Title</label>
						<input type="text" class="form-control col-8" name="site_title" value="" placeholder="Enter Site title" id="site_title">
					   </div> 
					    <div class="form-group">
						<label for="title">Copyright Text</label>
						<input type="text" class="form-control col-8" name="copyright_text" value="" placeholder="Enter Copyright Text" id="copy_text">
					   </div> 
					   
					    <!--div class="form-group">
						<label for="Logo ">Upload Site Logo </label><br>
							<img src="" height="100px" width="140px">
						<input type="file" class="form-control col-8" placeholder="Upload Site Logo" name="logo" id="logo">
						<small>Maximum upload file size: 2MB</small>
					   </div>  
					   <div class="form-group">
						<label for="Logo ">Upload Favicon </label><br>
						<img src="http://3.6.109.8/doctel/public/site_assets/images/1578903384.png" height="100px" width="140px">
						<input type="file" class="form-control col-8" placeholder="Upload favicon" name="favicon" id="favicon">
						<small>Maximum upload file size: 2MB</small>
					   </div---> 	  
					  </div>
						  <!--div role="tabpanel" class="tab-pane fade" id="social">
						  <h3>Social Links</h3>
						   <div class="form-group">
							<label for="title">Facebook</label>
							<input type="text" class="form-control col-8" name="facebook" value="" placeholder="Enter Facebook url" id="site_title">
						   </div> 
							<div class="form-group">
							<label for="title">Instagram</label>
							<input type="text" class="form-control col-8" name="instagram" value="" placeholder="Enter Instagram url" id="site_title">
						   </div> 
							<div class="form-group">
							<label for="Logo ">Twitter: </label>
							<input type="text" class="form-control col-8" name="twitter" value="" placeholder="Enter Twitter url" id="site_title">
						   </div> 
						   <div class="form-group">
							<label for="Logo ">Linkedin: </label>
							<input type="text" class="form-control col-8" name="linkedin" value="" placeholder="Enter Linkedin url" id="site_title">
						   </div>  
						  </div--> 
						 
						  
						  <!--div role="tabpanel" class="tab-pane fade" id="address">
						  <h3>Contact</h3>
						   <div class="form-group">
							<label for="title">Head Office Address</label>
							<input type="text" class="form-control col-8" name="head_office_address" value="" placeholder="Enter Address" id="site_title">
						   </div> 
							<div class="form-group">
							<label for="title">Head Office Phone </label>
							<input type="text" class="form-control col-8" name="head_office_phone" value="" placeholder="Enter Phone" id="site_title">
						   </div> 
							<div class="form-group">
							<label for="Logo ">Branch Office 1 Address: </label>
							<input type="text" class="form-control col-8" name="branch_office1_address" value="" placeholder="Enter Address" id="site_title">
						   </div> 
						   <div class="form-group">
							<label for="Logo ">Branch Office 1 Phone:</label>
							<input type="text" class="form-control col-8" name="branch_office1_phone" value="" placeholder="Enter Phone" id="site_title">
						   </div>  
						   <div class="form-group">
							<label for="Logo ">Branch Office 2 Address:</label>
							<input type="text" class="form-control col-8" name="branch_office2_address" value="" placeholder="Enter Address" id="site_title">
						   </div> 
						   <div class="form-group">
							<label for="Logo ">Branch Office 2 Phone: </label>
							<input type="text" class="form-control col-8" name="branch_office2_phone" value="" placeholder="Enter Phone" id="site_title">
						   </div>   
						   <div class="form-group">
							<label for="Logo ">Branch Office 3 Address: </label>
							<input type="text" class="form-control col-8" name="branch_office3_address" value="" placeholder="Enter Address" id="site_title">
						   </div> 
						   <div class="form-group">
							<label for="Logo ">Branch Office 3 Phone: </label>
							<input type="text" class="form-control col-8" name="branch_office3_phone" value="" placeholder="Enter Phone" id="site_title">
						   </div> 
						   <div class="form-group">
							<label for="Logo ">Contact Email: </label>
							<input type="text" class="form-control col-8" name="contact_email" value="" placeholder="Enter Contact Email" id="site_title">
						   </div>  
						  </div-->
						  
						<div class="form-group text-center">
							<div class="col-xs-12 pb-3">
								<button class="btn btn-block btn-lg btn-info col-2" type="submit">Save Settings</button>
							</div>
						</div>
					 <?php echo Form::close(); ?>

					</div>
					</form> 
                
                </div>
              </div>

</div>
<?php echo $__env->make('components.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>